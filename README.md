# Capacitación Oracle Forms

En este repositorio encontraran los recursos usados durante la capacitación de Oracle Forms.

El repositorio cuenta con las siguientes carpetas:

- [ ] Carpeta **RECURSOS**: Donde encontraran instaladores y cualquier otro programa que se valla a utilizar durante la capacitación.
- [ ] Carpeta **DIAPOSITIVAS**: Donde serán subidas las diapositivas que se usen en la capacitación y sirvan como material de referencia para los estudiantes.

```
Bienvenidos a la Capacitación de Oracle Forms. Sudamericana de Software.
```
## Videos
[Videos de la Capacitación](https://1drv.ms/u/s!Am-9a2wgXgDjhQknX9qF9TCHUSZe?e=3dUnSM)